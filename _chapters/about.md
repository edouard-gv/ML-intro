---
title: À propos
layout: chapter
part: références
order: 1
permalink: a-propos
---

Site libre à contenu statique et collaboratif.

Le but de ce projet est de proposer une introduction au Machine Learning. Ses deux spécificités sont
- concernant le contenu, il est à destination de celles et ceux qui ont fait des mathématiques, ont aimé ça, mais ont tout oublié ou presque
- concernant la forme, nous voulions explorer une nouvelle façon de collaborer autours d'outils de développement de code, et en proposant un parcours pas forcément linéaire, mais éditable néanmoins sous forme de livre

# Licence

Licence [CC BY-NC-SA 2.0 FR](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/) Attribution-NonCommercial-ShareAlike 4.0 International. Vous êtes libre d’utiliser et de modifier ces textes à condition de citer systématiquement sa provenance, de ne pas faire d’usage commercial de votre utilisation, et de partager votre version dans les mêmes conditions indiquées ici, donc avec la même licence.

Inspiration et éléments de code d'[Antoine Fauchié](https://memoire.quaternum.net/) et de [Tomas Parisot](https://oncletom.io/).

Icones de <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> accessibles sur <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>, licence <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>.

# Fabrication

Ce contenu est fabriqué avec les langages, composants, programmes et logiciels suivants :

- langage de balisage léger Markdown ;
- générateur de site statique Jekyll ;
- extensions pour Jekyll : jekyll-scholar et jekyll-microtypo ;
- système de gestion de versions Git ;
- plate-forme d’hébergement de dépôt Git GitLab ;
- déploiement continu et hébergement et CDN : Gitlab ;
- à venir: script paged.js pour la conversion HTML > PDF ;
- éditeur de texte et IDE : Atom;
