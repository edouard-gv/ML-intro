---
title: Enseigner l'informatique au lycée général
part: politique
layout: chapter
---
La différence entre les formations générales (comme le secondaire général) et les formations professionnalisantes (comme les écoles d'ingénieur) est que les premières cherchent à expliquer le monde, alors que les secondes apprennent à le changer.

Cela ne limite pas les fonctions de ces enseignements, la physique est aussi un moyen d'appréhender l'esprit scientifique par exemple.

Concernant l'informatique (ou les sciences du numériques, le nom est encore flou), on peut se poser les deux questions suivantes sur cette nouvelle matière enseignée au lycée.
- Quelle partie du monde voulons nous expliquer ?
- Y a-t-il une fonction supplémentaire à cet enseignement ?

Pour la première question, on y incluera l'algorithmique effectivement, mais aussi la place du libre arbitre dans un monde où les algorithmes sont capables de prendre des décisions meilleures dans notre intérêt ; la définition de l'identité lorsque ces mêmes algorithmes nous connaissent mieux que nous. Comprendre ce qu'est une base de données, mais aussi les implications d'avoir nos données personnelles chez des tiers, voire en dehors de l'Europe ; comparer jointures et croisements. Avoir une idée des composants d'un ordinateur, et peut-etre de leur coût ; comprendre un cycle CPU. Et par la suite les différents niveaux de langage. Avoir une idée des couches réseaux, ce qu'est une adresse IP, un DNS peut-être. S'il faut expliquer le monde, il faut expliquer Internet, non ? À 18 ans, tout bachelier ne devrait il pas comprendre les enjeux de la neutralité du Net ?

Quant au fonctions supplémentaires d'un tel enseignement, il pourrait aborder des fonctions cognitives : créativité / imaginaire ; usage des différents supports de communication ; développement du sens critique ; abstraction ; perception de soi, être capable de se raconter...
