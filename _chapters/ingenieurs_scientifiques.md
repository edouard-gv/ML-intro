---
title: Ingénieurs et scientifiques
part: épistémologie
layout: chapter
---

Un petit apparté sur la différence entre science et ingénierie : l'aphorisme qui permet de résumer cela est que la science nous permet d'expliquer le monde, alors que l'ingénierie le change (avec tous les inconvénients des aphorismes).

Cela fonctionne par exemple entre la science et l'ingénieurie informatique (ou génie logiciel dans un sens plus restreint), entre la physique théorique et les différentes ingénieuries qui en découle (aéronautique, BTP), entre la chimie et le génie chimique (pétrochimie).

Cela peut-être une approche permettant de comprendre la différence entre [les formations générales et les formations professionnalisantes]({{ site.baseurl }}{% link _chapters/enseignement_general_ou_professionnel.md %}).

Ainsi, le contexte du Machine Learning n'est pas tant un objectif de scientifique : découvrir le réel, mais plutôt d'ingénieur : arriver à le manipuler. Sans forcément dire que l'une des approches est meilleures que l'autre, de toutes façons l'une est indispensables à l'autre. Et, bien sûr, ceux qui étudient les conditions d'existance du Machine Learning le font dans un objectif scientifique de découvrir l'univers.
