---
title: Oublier les inversions de matrice
part: introduction
order: 5
layout: chapter
---

# Retourner sa tête d'algébreux linéaire quand on n'a pas fait de statistiques

Si f a p variables, avec p mesures, on sait qu'on pourra *a priori* trouver f, et ainsi calculer f(y) pour des y qu'on a pas mesurés.
De même on sait *a priori* faire passer un polynôme de degré p par p points du plan. Et ce cas se rapporte au cas précédent.

Et on se concentre sur le cas linéaire où f se présente sur la forme sigma des aixi.
On a étudié en long et en large, du collège à la License. Et qui revient à inverser une matrice. Avec uniquement ces trois possibilités : il a exactement une solution, il n'y a aucune solution, il a une infinité de solutions.

Pour le formaliser :
- si A est le vecteur des ai, coefficient de f
- si X est la matrice des p caractéristiques dans lesquelles ont été effectuées les p mesures
- si Y est le vecteur des p mesures

Alors nos p mesures s'expriment sous la forme Y=XA, et A vaut donc X-1Y.

*A priori*, c'est à dire si A est inversible, de rang p etc.

Le Machine Learning nous permet de reproduire le comportement de f sans avoir à connaître les coefficient A de f, et donc sans avoir à inverser X.
