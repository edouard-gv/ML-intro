---
  title: posons le problème
  part: introduction
  order: 1
  layout: chapter
---

# Décrire l'univers

Le machine learning fait partie des techniques dont le but est de reproduire un phénomène complexe grâce à l'usage d'une machine. On aura appris à cette machine à répéter le phénomène, nous permettant de lui demander de l'appliquer sur de nouvelles données d'entrée, et ainsi à le prédire, ou à le reproduire.

Par exemple, on pourra demander à une machine météorilogique de nous prédire le temps à venir à partir des données actuelles. Auparavant, on lui aura appris à le faire, car il ne s'agit pas de retrouver dans le passé un situation exactement égale à celle du présent, cela n'existe pas, mais d'appliquer un modèle déduit du passé.

Ou encore, on pourra demander à une machine à reconnaissance faciale de retrouver un visage dans une photo de foule, sachant que dans cette photo de foule, c'est la première fois qu'on lui présente ce visage sous cette forme, mais qu'on lui a appris à reconnaître des visages à partir de photo de celui-ci.

Enfin, on pourra demander à une machine quel risque d'une pathologie particulière peut présenter tel nouveau genôme, à partir de ce qu'on sait d'autres génôme et des pathologies que leurs porteurs ont présentées.

Plus généralement, disons qu'on a un phénomène f, qui dans les conditions x, donne le résultat y, "y=f(x)". Habituellemet, le fait d'avoir plein de xi et les yi qui correspondent nous permet *de trouver f*, ce qui nous permettra de l'appliquer à de nouveaux x pour trouver les y correspondants.

Jusqu'à présent, on nous a donc habitué à chercher f. Le machine learning nous demande si on en n'a vraiment besoin. Et si le véritable besoin était de pouvoir calculer f(x) pour de nouveau x, sans forcément connaître précisément f? Le Machine Learning nous invite à retourner le problème, à construire une boîte noire censée fonctionner au plus près de f, qui permettra de calculer y pour de nouveaux x.

Bien-sûr, on ne peut le faire que quand on se fiche de f, par exemple d'avoir à lui donner du sens. On est plus dans une approche [d'ingénieur que de scientifique]({{ site.baseurl }}{% link _chapters/ingenieurs_scientifiques.md %}). C'est quand la donnée est préférée à la connaissance (Data over Knowledge).

Jusqu'à présent, deux grands domaines nous permettaient de trouver f, celui de l'algèbre linaire et du calcul matriciel, et celui des statistiques. Le Machine Learning nous permet donc [d'éviter les inversions de matrices]({{ site.baseurl }}{% link _chapters/oublier_les_inversions_de_matrice.md %}) dans le premier cas, et [d'éviter de calculer variance et covariance]({{ site.baseurl }}{% link _chapters/oublier_les_calculs_de_covariance.md %}) dans le second.

En revanche, il exige, comme en statistique, de [valider la précision de son estimation]({{ site.baseurl }}{% link _chapters/protocole_scientifique_statistique.md %}), ce qui est souvent oublié en informatique.
